#!/bin/bash

build_dev_image_with_push () {

    mkdir -p /kaniko/.docker
    echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    echo "-----------DOCKER BUILD STARTED. IMAGE WILL BE PUSHED-------------" 
    /kaniko/executor --context "${CI_PROJECT_DIR}" --dockerfile "${CI_PROJECT_DIR}"/Dockerfile --destination "${CI_REGISTRY_IMAGE}":feature-"${CI_COMMIT_SHORT_SHA}" --build-arg http_proxy="${http_proxy}" --build-arg https_proxy="${https_proxy}" --build-arg no_proxy="${no_proxy}"
}

prod_build_image () {

    mkdir -p /kaniko/.docker
    echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    echo "---------DOCKER BUILD STARTED------------"
    /kaniko/executor --context "${CI_PROJECT_DIR}" --dockerfile "${CI_PROJECT_DIR}"/Dockerfile --destination "${CI_REGISTRY_IMAGE}":"${TAG}" --build-arg http_proxy="${http_proxy}" --build-arg https_proxy="${https_proxy}" --build-arg no_proxy="${no_proxy}"
}