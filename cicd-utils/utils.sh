#!/bin/bash

error_handling () {

    rc=$?
    if [ ${rc} -eq 1 ] ; then
     echo ${rc}
     exit 1
    fi
}

get_tag_id () {
     apt-get update -y
     apt-get install git=1:2.20.1-2+deb10u3 -y
     LAST_BUILD_ID=$(git tag -l  | sort -V | tail -n 1  | cut -c 6,7,8 )
      echo "LAST_BUILD_ID=${LAST_BUILD_ID}"
      if [ -n "${LAST_BUILD_ID}" ] ; then
          BUILD_INDEX=$(( LAST_BUILD_ID + 1 ))
          export TAG="v1.0.${BUILD_INDEX}" 
          echo  "TAG=${TAG}"
          echo  "TAG=${TAG}" > id.env
      else
           BUILD_INDEX=v1.0.0
           export TAG="${BUILD_INDEX}" 
           echo  "TAG=${TAG}" 
           echo  "TAG=${TAG}" > id.env
      fi
}

lint_shell () {

    echo "------------SHELLCHEK .SH or .BASH FILES----------"
    while IFS= read -r -d '' file
    do
      shellcheck "${file}"
      error_handling
    done <   <(find "${CI_PROJECT_DIR}" -name "*.sh" -print0 -o -name "*.bash" -print0)
   
}

hadolint_dockerfile () {

    echo "------------HADOLINT DOCKERFILE----------"
    hadolint "${CI_PROJECT_DIR}"/Dockerfile
    error_handling
}

tag_release () {
    apt-get update -y
    apt-get install curl=7.64.0-4+deb10u2 -y
    curl -o /dev/null -w "%{http_code}" -X POST -H "PRIVATE-TOKEN:$CSC_API_TOKEN" --fail "$CI_API_V4_URL/projects/$CI_PROJECT_ID/repository/tags?tag_name=$TAG&ref=${CI_COMMIT_SHORT_SHA}"
    error_handling
}

help_usage () {

    # Display Help
   echo "This is a help function "
   echo "Syntax: scriptTemplate [get_tag_id|lint_shell|hadolint_dockerfile|build_dev_image_no_push|build_dev_image_with_push|prod_build_image|tag_release]"
   echo "options:"
   echo "help                          Print this help"
   echo "get_tag_id                    Generate tag id for current build. This will be used for tag_release"
   echo "lint_shell                    shellchecker for bash or sh script (if sh or bash script exists)"
   echo "hadolint_dockerfile           hadolint for dockerfile"
   echo "tag_release                   tag current build with generated tag id "
}

case "$1" in
        get_tag_id|lint_shell|hadolint_dockerfile|tag_release)
             "$*"  
             ;;
        help)
                help_usage
                ;;
        *)
                echo  "Unkown Command."
                help_usage
                ;;
esac