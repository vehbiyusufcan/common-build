#!/bin/bash
# initial tools
apt-get update \
    && apt-get \
               --no-install-recommends \
               install -y \
                  openssh-client=1:7.9p1-10+deb10u2 \
                  sshpass=1.06-1 \
                  curl=7.64.0-4+deb10u2 \
                  jq=1.5+dfsg-2+b1 \
                  rsync=3.1.3-6 \
                  git=1:2.20.1-2+deb10u3 \
                  unzip=6.0-23+deb10u2 \
                  shellcheck=0.5.0-3 \
                  bats=0.4.0-1.1 \
                  ca-certificates=20200601~deb10u2 \
                  zip=3.0-11+b1 \
    && rm -rf /var/lib/apt/*

#Hadolint
curl -L \
     "https://github.com/hadolint/hadolint/releases/download/v2.4.0/hadolint-Linux-x86_64" \
     -o /usr/bin/hadolint && chmod u+x /usr/bin/hadolint
